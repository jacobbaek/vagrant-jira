## What is purpose?
Install Jira using Vagrant

## Environment
- Provider : KVM(tested ubuntu 19.04), Virtualbox(tested windows 10, ubuntu 19.04)
- Provisioner : Ansible
- Box : Centos/7

## Keep to know
- If you use vagrant on Windows, you should install vagrant-guest_ansible plugin
  - https://github.com/vovimayhem/vagrant-guest_ansible
  run the command before you run the "vagrant up"
  vagrant plugin install vagrant-guest_ansible; vagrant up
- If you need to re-run the ansible by "vagrant provision",
  you should run "vagrant reload --provision-with" before the "vagrant provision"

## How to use
Run "vagrant up" in the directory which is with Vagrantfile
After done, You can see the Jira first setup page at the localhost:8080.

If you wanna own database setup, you can setup with Postgres DB.
This playbook already has postgresql 9.6 server.

You can set Database through below DB information.
- DB host: 127.0.0.1
- DB name: jiradb
- DB user: jiradbuser
- DB password: jiradbuser

## Final stage
You can access http://localhost:8015 and you can find the Jira initial page.
There is no license file. so you should register trial license in your atlassian homepage.
refer to https://confluence.atlassian.com/adminjiraserver/installing-jira-applications-on-linux-from-archive-file-938846844.html
So, you should register your Jira and get the trial license.